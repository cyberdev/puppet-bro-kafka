bro-kafka

# Nothing here yet, just a placeholder

The development is happening in the 'develop' branch.

This is the bro-kafka module. This should be used with other modules that manage
elasticsearch, logstash, kafka, and bro, to configure the data pipeline between 
them.

License
-------
Apache License

Contact
-------
Derek Ditch <derek@criticalstack.com>

Support
-------

Please log tickets and issues at our [Projects site](http://projects.example.com)
